import axios from "axios";

export async function getRanking(dealerUrl) {
  const { data } = await axios({
    method: "get",
    url: dealerUrl + "/ranking",
    headers: {
      "ngrok-skip-browser-warning": `true`,
    },
  });
  return data;
}

export async function getNewRound(dealerUrl) {
  const { data } = await axios({
    method: "post",
    url: dealerUrl + "/rounds",
    headers: {
      "ngrok-skip-browser-warning": `true`,
    },
  });
  return data;
}

export async function runMatch(dealerUrl, matchId) {
  const { data } = await axios({
    method: "post",
    url: dealerUrl + "/run-match/" + matchId,
    headers: {
      "ngrok-skip-browser-warning": `true`,
    },
  });
  return data;
}
