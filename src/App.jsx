import { useState, useRef } from "react";
import { sha256 } from "js-sha256";
import { getRanking, getNewRound, runMatch } from "./api";

import {
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
} from "@mui/material";

import "./App.css";

const App = () => {
  const [matches, setMatches] = useState([]);
  const [players, setPlayers] = useState([]);
  const dealerApiRef = useRef('https://0e7a-2804-29b8-513b-34a-24-4e5d-4a8b-32c7.ngrok-free.app/api');

  const handleRankingUpdate = () => {
    getRanking(dealerApiRef.current)
      .then(res => {
        setPlayers(res);
      })
  }

  const handleGetNewRound = () => {
    getNewRound(dealerApiRef.current)
      .then(res => {
        setMatches(res?.matches)
      })
  }

  const handleRunMatch = (matchId) => {
    runMatch(dealerApiRef.current, matchId)
      .then(res => {
        let updatedMatches = matches
        updatedMatches.map((match, index) => {
          if (match.id == matchId) {
            updatedMatches[index] = res
          }
        })
        setMatches(updatedMatches);
        localStorage.setItem("matches", JSON.stringify(updatedMatches))
      }).finally(() => handleRankingUpdate())
  }

  const getPlayerName = (playerId) => {
    return players?.find(player => player.id === playerId).name
  }

  return (
    <div style={{ display: "flex", minWidth: "100%" }}>
      <div style={{ width: "50%" }}>
        <form action="/action_page.php">
          <label htmlFor="dealerUrl">
            <b>DEALER API URL</b>:{" "}
          </label>
          <input type="text" value="https://0e7a-2804-29b8-513b-34a-24-4e5d-4a8b-32c7.ngrok-free.app/api" id="dealerUrl" name="dealerUrl" onChange={(e) => { dealerApiRef.current = e.target.value }} />
          <br />
          <br />
        </form>
        <h1>Tabela do Torneio</h1>
        <List
          sx={{
            width: "100%",
            maxWidth: 1200,
            bgcolor: "rgb(40, 40, 40)",
          }}
        >
          {players
            ?.map((player) => {
              return (
                <ListItem divider key={player.name} sx={{ height: "100px" }}>
                  <ListItemAvatar>
                    <Avatar
                      sx={{
                        height: "70px",
                        width: "70px",
                        marginLeft: "10px",
                        marginRight: "20px",
                      }}
                    >
                      <img
                        src={
                          "https://gravatar.com/avatar/" +
                          sha256(player.id.trim())
                        }
                        alt="Player"
                        style={{
                          height: "66px",
                          border: "2px solid rgb(224, 224, 224)",
                          borderRadius: "50%",
                        }}
                      />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={player.name}
                    primaryTypographyProps={{ fontSize: "30px" }}
                    sx={{ width: "80%" }}
                  />
                  <ListItemText
                    primary={player.balance + " pontos"}
                    primaryTypographyProps={{
                      fontSize: "20px",
                      color:
                        player.balance > 0
                          ? "rgb(73, 186, 84)"
                          : "rgb(186, 73, 73)",
                    }}
                  />
                </ListItem>
              );
            })}
        </List>
        <br />
        <button type="button" onClick={() => handleRankingUpdate()} >Atualizar Ranking!</button>
        <br />
      </div>
      <div style={{ width: "1%" }}></div>
      <div style={{ width: "50%" }}>
        <h2>Partidas da Rodada</h2>
        <List
          sx={{
            width: "100%",
            maxWidth: 1200,
            bgcolor: "rgb(40, 40, 40)",
          }}
        >
          {matches?.map(match => {
            return (
              <ListItem divider key={match.id + match.player1Id} sx={{ height: "80px" }}>
                <ListItemAvatar>
                  <Avatar
                    sx={{
                      height: "60px",
                      width: "60px",
                      marginLeft: "10px",
                      marginRight: "20px",
                    }}
                  >
                    <img
                      src={
                        "https://gravatar.com/avatar/" +
                        sha256(match.player1Id)
                      }
                      alt="Player"
                      style={{
                        height: "56px",
                        border: "2px solid rgb(224, 224, 224)",
                        borderRadius: "50%",
                      }}
                    />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={" vs "}
                  sx={{ maxWidth: "25px" }}
                />
                <ListItemAvatar>
                  <Avatar
                    sx={{
                      height: "60px",
                      width: "60px",
                      marginLeft: "10px",
                      marginRight: "20px",
                    }}
                  >
                    <img
                      src={
                        "https://gravatar.com/avatar/" +
                        sha256(match.player2Id)
                      }
                      alt="Player"
                      style={{
                        height: "56px",
                        border: "2px solid rgb(224, 224, 224)",
                        borderRadius: "50%",
                      }}
                    />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={getPlayerName(match.player1Id) + " (p1) vs " + getPlayerName(match.player2Id) + " (p2)"}
                  secondary={match.result ? "Resultado: " + match.result : "Falta jogar"}
                  primaryTypographyProps={{ fontSize: "17px" }}
                  sx={{ minWidth: "100px" }}
                  secondaryTypographyProps={{ color: "grey" }}
                />
                {match.result ?
                  <>
                    <ListItemText
                      primary={"Mão p1: " + match?.player1FinalHand}
                      primaryTypographyProps={{
                        fontSize: "12px",
                        width: "170px",
                        minWidth: "170px",
                      }}
                    />
                    <ListItemText
                      primary={"Mão p2: " + match?.player2FinalHand}
                      primaryTypographyProps={{
                        fontSize: "12px",
                        width: "165px",
                        minWidth: "165px",
                      }}
                    />
                    <ListItemText
                      primary={"Aposta: " + match?.bet}
                      primaryTypographyProps={{
                        fontSize: "12px",
                        marginRight: "15px",
                        width: "60px",
                        maxWidth: "60px",
                      }}
                    /></> : ""}
                <button type="button" onClick={() => handleRunMatch(match.id)} disabled={match.result}>Fight!</button>
              </ListItem>
            )
          })}
        </List>
        <br />
        <button type="button" onClick={() => handleGetNewRound()}>Gerar nova rodada</button>
      </div >
    </div>
  );
};
export default App;
